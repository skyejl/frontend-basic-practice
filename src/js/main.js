import person from './person';
import $ from 'jquery';

const URL = 'http://localhost:3000/person';
fetch(URL)
  .then(res => res.json())
  .then(result => {
    const aperson = new person(result.name, result.age, result.description);
    let personDescp = aperson.description;
    $('.profile-descrip').html(personDescp);
    let educations = result.educations;
    let listarr = educations.map(e => {
      return (
        `<h3 class="year">${e.year}</h3>` +
        `<div class="intro">` +
        `<h3 class="title">${e.title}</h3>` +
        `<p class="description">${e.description}</p>` +
        `</div>`
      );
    });
    listarr.map(item => {
      $('ul').append(`<li class="list">${item}</li>`);
    });
  });

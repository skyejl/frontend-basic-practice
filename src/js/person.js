export default class Person {
  constructor(name, age, description) {
    this.name = name;
    this.age = age;
    this.description = description;
  }
}
